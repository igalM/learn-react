import { Comment } from "../types/Comment";

type Props = {
    comment: Comment;
}

export default function CommentItem({ comment }: Props) {
    const authorProfile = `/profile/:${comment.author.username}`;
    return (
        <div className="card">
            <div className="card-block">
                <p className="card-text">{comment.body}</p>
            </div>
            <div className="card-footer">
                <a href={authorProfile} className="comment-author">
                    <img src={comment.author.image} className="comment-author-img" alt="comment-author-img" />
                </a>
&nbsp;
<a href={authorProfile} className="comment-author">{comment.author.username}</a>
                <span className="date-posted">{comment.createdAt}</span>
            </div>
        </div>
    )
}