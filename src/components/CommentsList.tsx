import { Comment } from "../types/Comment";
import CommentItem from "./CommentItem";

type Props = {
    comments: Comment[];
}

export default function CommentsList({ comments }: Props) {
    if (!comments.length) return null;

    const list = comments.map((comment, index) => <CommentItem key={index} comment={comment}></CommentItem>);

    return <>
        {list}
    </>
}