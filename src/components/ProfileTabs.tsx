import { useEffect, useState } from "react";
import useGlobalArticles from "../hooks/useGlobalArticles";
import { Article } from "../types/Article";
import ArticlesList from "./ArticlesList";
import TabItem from "./TabItem";

type Props = {
    defaultTab: string;
    username: string;
}

export default function ProfileTabs({ defaultTab, username }: Props) {
    const [activeTab, setActiveTab] = useState<string>(defaultTab);
    const [currentArticles, setCurrentArticles] = useState<Article[]>([]);
    const { isLoading: myArticlesLoading, data: myArticlesData } = useGlobalArticles({ enabled: activeTab === 'my-articles', author: username, queryKey: `${username}_articles` });
    const { isLoading: favoritedArticlesLoading, data: favoritedArticlesData } = useGlobalArticles({ enabled: activeTab === 'favorited-articles', favorited: username, queryKey: `${username}_favorited_articles` });

    useEffect(() => {
        let articles: Article[] = [];
        const myArticles = myArticlesData?.articles || [];
        const favoritedArticles = favoritedArticlesData?.articles || [];
        if (myArticles || favoritedArticles) {
            switch (activeTab) {
                case 'my-articles':
                    articles = myArticles;
                    break;
                case 'favorited-articles':
                    articles = favoritedArticles;
                    break;
                default:
                    articles = [];
            }
        }
        setCurrentArticles(articles);
    }, [myArticlesData, favoritedArticlesData, activeTab]);

    return (
        <>
            <div className="feed-toggle">
                <ul className="nav nav-pills outline-active">
                    <TabItem id="my-articles" title="My Articles" activeTab={activeTab} setActiveTab={setActiveTab}></TabItem>
                    <TabItem id="favorited-articles" title="Favorited Articles" activeTab={activeTab} setActiveTab={setActiveTab}></TabItem>
                </ul>
            </div>
            <ArticlesList loading={myArticlesLoading || favoritedArticlesLoading} articles={currentArticles}></ArticlesList>
        </>
    )
}