type Props = {
    hidden?: boolean;
    id: string;
    title: string;
    activeTab: string;
    setActiveTab?: (id: string) => void;
}

export default function TabItem({ hidden = false, id, title, activeTab, setActiveTab }: Props) {

    const handleClick = () => {
        if (setActiveTab) {
            setActiveTab(id);
        }
    };

    if (hidden) return null;

    return (
        <li
            className="nav-item">
            <button onClick={handleClick}
                className={`nav-link ${activeTab === id ? 'active' : ''}`}> {title}</button>
        </li>
    );
};
