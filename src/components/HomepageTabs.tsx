import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../contexts/auth-context";
import useFeedArticles from "../hooks/useFeedArticles";
import useGlobalArticles from "../hooks/useGlobalArticles";
import { Article } from "../types/Article";
import ArticlesList from "./ArticlesList";
import TabItem from "./TabItem";

type Props = {
    defaultTab: string;
    activeTag: string;
}

export default function HomepageTabs({ defaultTab, activeTag }: Props) {
    const { user } = useAuthContext();
    const [activeTab, setActiveTab] = useState(defaultTab);
    const [currentArticles, setCurrentArticles] = useState<Article[]>([]);
    const [filterByTag, setFilterByTag] = useState<string>(activeTag);

    const { isLoading: feedLoading, data: feedArticlesData } = useFeedArticles({ enabled: activeTab === 'my-feed' });
    const { isLoading: globalLoading, data: globalArticlesData } = useGlobalArticles({ enabled: activeTab === 'global', queryKey: 'global_articles' });
    const { isLoading: byTagLoading, data: byTagArticlesData } = useGlobalArticles({ enabled: activeTab === 'tag-articles', tag: filterByTag, queryKey: `#${filterByTag}_articles` });

    const navigate = useNavigate();

    const myFeedHandler = (id: string) => {
        if (!user) return navigate('/login');
        setActiveTab(id);
        setFilterByTag('');
    }

    const globalFeedHandler = (id: string) => {
        setActiveTab(id);
        setFilterByTag('');
    }

    useEffect(() => {
        if (activeTag) {
            setActiveTab('tag-articles');
            setFilterByTag(activeTag);
        }
    }, [activeTag, setActiveTab, setFilterByTag]);

    useEffect(() => {
        let articles: Article[] = [];
        const feedArticles = feedArticlesData?.articles || [];
        const globalArticles = globalArticlesData?.articles || [];
        const articlesByTag = byTagArticlesData?.articles || [];
        if (feedArticles || globalArticles) {
            switch (activeTab) {
                case 'my-feed':
                    articles = feedArticles;
                    break;
                case 'global':
                    articles = globalArticles;
                    break;
                case 'tag-articles':
                    articles = articlesByTag;
                    break;
                default:
                    articles = [];
            }
        }
        setCurrentArticles(articles);
    }, [feedArticlesData, globalArticlesData, byTagArticlesData, activeTab]);

    return (
        <>
            <div className="feed-toggle">
                <ul className="nav nav-pills outline-active">
                    <TabItem id="my-feed" title="Your Feed" activeTab={activeTab} setActiveTab={myFeedHandler}></TabItem>
                    <TabItem id="global" title="Global Feed" activeTab={activeTab} setActiveTab={globalFeedHandler}></TabItem>
                    <TabItem hidden={!filterByTag} id="tag-articles" title={`#${filterByTag}`} activeTab={activeTab}></TabItem>
                </ul>
            </div>
            <ArticlesList loading={feedLoading || globalLoading || byTagLoading} articles={currentArticles}></ArticlesList>
        </>
    )
}