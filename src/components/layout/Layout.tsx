import { lazy, Suspense, useEffect } from "react";
import {
    Routes,
    Route,
    Navigate,
} from "react-router-dom";
import Footer from '../../components/layout/Footer';
import Header from '../../components/layout/Header';
import ProtectedRoute from "../../components/routing/ProtectedRoute";
import { useAuthContext } from "../../contexts/auth-context";
import useGetUser from "../../hooks/useGetUser";
import { storage } from "../../utils/utils";
import Spinner from "../common/Spinner";
const ArticlePage = lazy(() => import("../../pages/article/ArticlePage"));
const EditorPage = lazy(() => import("../../pages/editor/EditorPage"));
const HomePage = lazy(() => import("../../pages/home/HomePage"));
const LoginPage = lazy(() => import("../../pages/login/LoginPage"));
const ProfilePage = lazy(() => import("../../pages/profile/ProfilePage"));
const RegisterPage = lazy(() => import("../../pages/register/RegisterPage"));
const SettingsPage = lazy(() => import("../../pages/settings/SettingsPage"));

export default function Layout() {
    const { setUser } = useAuthContext();
    const token = storage.getToken();

    const { data } = useGetUser({ enabled: token !== null, token: token || '' })

    useEffect(() => {
        if (data) {
            setUser(data.user);
        }
    }, [data, setUser]);


    return (
        <Suspense fallback={<Spinner />}>
            <Header></Header>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/editor" element={
                    <ProtectedRoute>
                        <EditorPage />
                    </ProtectedRoute>
                } />
                <Route path="/article/:slug" element={
                    <ProtectedRoute>
                        <ArticlePage />
                    </ProtectedRoute>
                } />
                <Route path="/profile/:username" element={
                    <ProtectedRoute>
                        <ProfilePage />
                    </ProtectedRoute>
                } />
                <Route path="/settings" element={
                    <ProtectedRoute>
                        <SettingsPage />
                    </ProtectedRoute>
                } />
                <Route
                    path="*"
                    element={<Navigate to="/" replace />}
                />
            </Routes>
            <Footer></Footer>
        </Suspense>
    )
}