import { useParams } from "react-router-dom";
import ProfileTabs from "../../components/ProfileTabs";
import { useAuthContext } from "../../contexts/auth-context";
import useProfile from "../../hooks/useProfile";
import { Profile } from "../../types/Profile";

export default function ProfilePage() {
    const params = useParams();
    const { user } = useAuthContext();
    const { data, isLoading } = useProfile({ enabled: params.username !== user.username, username: params.username!! });

    if (isLoading) return <p>Loading...</p>

    let profileData: Profile = null!;

    if (data?.profile) {
        profileData = data?.profile;
    }

    let actionButton = <button className="btn btn-sm btn-outline-secondary action-btn">
        <i className="ion-plus-round"></i>
        Follow {params.username}
    </button>;

    if (user.username === params.username) {
        profileData = {
            username: user.username,
            bio: user.bio,
            image: user.image,
            following: false
        };
        actionButton = <a className="btn btn-sm btn-outline-secondary action-btn" href="/settings">
            <i className="ion-gear-a"></i> Edit Profile Settings
        </a>
    }

    return (
        <div className="profile-page">
            <div className="user-info">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-10 offset-md-1">
                            <img src={profileData.image} className="user-img" alt="avatar" />
                            <h4>{profileData.username}</h4>
                            <p>{profileData.bio}</p>
                            {actionButton}
                        </div>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-md-10 offset-md-1">
                        <ProfileTabs defaultTab="my-articles" username={profileData.username}></ProfileTabs>
                    </div>
                </div>
            </div>
        </div>
    );
}