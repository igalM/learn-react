import { useState } from "react";
import Tabs from "../../components/HomepageTabs";
import TagsList from "../../components/TagsList";
import useTags from "../../hooks/useTags"

export default function HomePage() {
    const [filterByTag, setFilterByTag] = useState<string>('');
    const { isLoading: tagsLoading, data: tagsData } = useTags();

    const tags = tagsData?.tags || [];

    return (
        <div className="home-page">

            <div className="banner">
                <div className="container">
                    <h1 className="logo-font">conduit</h1>
                    <p>A place to share your knowledge.</p>
                </div>
            </div>

            <div className="container page">
                <div className="row">
                    <div className="col-md-9">
                        <Tabs defaultTab="global" activeTag={filterByTag} />
                    </div>
                    <div className="col-md-3">
                        <TagsList loading={tagsLoading} tags={tags} filterByTag={setFilterByTag}></TagsList>
                    </div>
                </div>
            </div>
        </div>
    );
}