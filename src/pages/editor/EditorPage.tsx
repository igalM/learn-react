import { useForm, SubmitHandler } from "react-hook-form";
import useCreateArticle from "../../hooks/useCreateArticle";
import { CreateArticleRequest } from "../../types/Article";

type Inputs = {
    title: string,
    description: string,
    body: string,
    tagList: string
};

export default function EditorPage() {
    const { mutate } = useCreateArticle();
    const { register, handleSubmit } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = data => {
        const article: CreateArticleRequest = {
            article: {
                ...data,
                tagList: data.tagList.split(',')
            }
        }
        mutate(article);
    };

    return (
        <div className="editor-page">
            <div className="container page">
                <div className="row">
                    <div className="col-md-10 offset-md-1 col-xs-12">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <fieldset>
                                <fieldset className="form-group">
                                    <input {...register('title')} type="text" className="form-control form-control-lg" placeholder="Article Title" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <input {...register('description')} type="text" className="form-control" placeholder="What's this article about?" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <textarea {...register('body')} className="form-control" rows={8}
                                        placeholder="Write your article (in markdown)"></textarea>
                                </fieldset>
                                <fieldset className="form-group">
                                    <input {...register('tagList')} type="text" className="form-control" placeholder="Enter tags (seperated by comma)" />
                                    <div className="tag-list"></div>
                                </fieldset>
                                <button className="btn btn-lg pull-xs-right btn-primary" type="submit">
                                    Publish Article
                        </button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}