import { useState } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import CommentsList from "../../components/CommentsList";
import { useAuthContext } from "../../contexts/auth-context";
import useArticle from "../../hooks/useArticle";
import useComments from "../../hooks/useComments";
import useCreateComment from "../../hooks/useCreateComment";
import { CreateCommentRequest } from "../../types/Comment";

export default function ArticlePage() {
    const { slug } = useParams();
    const { user } = useAuthContext();
    const { isLoading, data } = useArticle({ slug });
    const { data: commentsData } = useComments({ slug });
    const { mutate } = useCreateComment();

    const [comment, setComment] = useState('');

    const comments = commentsData?.comments || [];

    const submitComment = () => {
        const commentData: CreateCommentRequest = {
            slug: slug || '',
            comment: {
                body: comment
            }
        }
        setComment('');
        mutate(commentData);
    }

    if (isLoading) return <p>Loading Article...</p>

    const article = data?.article;

    return (
        <div className="article-page">
            <div className="banner">
                <div className="container">
                    <h1>{data?.article.title}</h1>
                    <div className="article-meta">
                        <Link to={`/profile/${article?.author.username}`}><img src={article?.author.image} alt="author-img" /></Link>
                        <div className="info">
                            <Link to={`/profile/${article?.author.username}`} className="author">{article?.author.username}</Link>
                            <span className="date">{article?.createdAt}</span>
                        </div>
                        <button className="btn btn-sm btn-outline-secondary">
                            <i className="ion-plus-round"></i>
                &nbsp;
                Follow {article?.author.username}
                        </button>
            &nbsp;&nbsp;
            <button className="btn btn-sm btn-outline-primary">
                            <i className="ion-heart"></i>
                &nbsp;
                Favorite Post <span className="counter">({article?.favoritesCount})</span>
                        </button>
                    </div>

                </div>
            </div>

            <div className="container page">

                <div className="row article-content">
                    <div className="col-md-12">
                        <p>{article?.body}</p>
                    </div>
                </div>
                <hr />

                {/* <div className="article-actions">
                    <div className="article-meta">
                        <a href="profile.html"><img src="http://i.imgur.com/Qr71crq.jpg" /></a>
                        <div className="info">
                            <a href="" className="author">Eric Simons</a>
                            <span className="date">January 20th</span>
                        </div>

                        <button className="btn btn-sm btn-outline-secondary">
                            <i className="ion-plus-round"></i>
                &nbsp;
                Follow Eric Simons
            </button>
            &nbsp;
            <button className="btn btn-sm btn-outline-primary">
                            <i className="ion-heart"></i>
                &nbsp;
                Favorite Post <span className="counter">(29)</span>
                        </button>
                    </div>
                </div> */}

                <div className="row">

                    <div className="col-xs-12 col-md-8 offset-md-2">

                        <form className="card comment-form">
                            <div className="card-block">
                                <textarea value={comment} onChange={(e) => setComment(e.target.value)} className="form-control" placeholder="Write a comment..." rows={3}></textarea>
                            </div>
                            <div className="card-footer">
                                <img src={user.image} className="comment-author-img" alt="comment-author-img" />
                                <button type="button" onClick={submitComment} className="btn btn-sm btn-primary">
                                    Post Comment
                    </button>
                            </div>
                        </form>
                        <CommentsList comments={comments}></CommentsList>
                    </div>
                </div>
            </div>
        </div>
    );
}