import axios from 'axios';
import { useQuery } from 'react-query';
import { GetArticleResponse } from '../types/Article';

const getArticle = async (slug: string) => {
    const { data } = await axios.get(`/articles/${slug}`);
    return data;
};

export default function useArticle({ slug = '' }) {
    return useQuery<GetArticleResponse, Error>(["article", slug], () => getArticle(slug));
}