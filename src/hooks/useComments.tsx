import axios from 'axios';
import { useQuery } from 'react-query';
import { MultipleCommentsResponse } from '../types/Comment';

const getComments = async (slug: string) => {
    const { data } = await axios.get(`/articles/${slug}/comments`);
    return data;
};

export default function useComments({ slug = '' }) {
    return useQuery<MultipleCommentsResponse, Error>(["comments", slug], () => getComments(slug));
}