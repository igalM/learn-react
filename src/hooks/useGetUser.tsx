import { useQuery } from "react-query";
import axios from "axios";
import { withJWTHeaders } from "../utils/utils";

type Props = {
    enabled: boolean;
    token: string;
}

export default function useGetUser({ token, enabled }: Props) {

    const getUserByToken = async () => {
        const { data } = await axios.get('/user', { headers: withJWTHeaders(token) });
        return data;
    };

    return useQuery<any, Error>(["user"], getUserByToken, {
        enabled: enabled,
        refetchOnWindowFocus: false,
        suspense: true
    });

}