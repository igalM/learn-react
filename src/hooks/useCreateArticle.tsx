import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { useNavigate } from "react-router";
import { CreateArticleRequest, GetArticleResponse } from "../types/Article";

export default function useCreateArticle() {
    const queryClient = useQueryClient();
    const navigate = useNavigate();

    const createArticle = async (article: CreateArticleRequest) => {
        const { data } = await axios.post('/articles', article);
        return data;
    };

    return useMutation(createArticle, {
        onSuccess: (data: GetArticleResponse) => {
            queryClient.setQueryData([`article_${data.article.slug}`], data);
            navigate(`/article/${data.article.slug}`);
        }
    });
}