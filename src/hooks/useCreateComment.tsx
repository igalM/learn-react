import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { CreateCommentRequest } from "../types/Comment";

export default function useCreateComment() {
    const queryClient = useQueryClient();

    const createComment = async (req: CreateCommentRequest) => {
        const { data } = await axios.post(`/articles/${req.slug}/comments`, { comment: req.comment });
        return data;
    }

    return useMutation(createComment, {
        onSuccess: () => {
            queryClient.invalidateQueries('comments');
        }
    });
}