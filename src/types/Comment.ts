import { Author } from "./Author";

export interface Comment {
    id: number;
    createdAt: string;
    updatedAt: string;
    body: string;
    author: Author;
}

export interface CreateCommentRequest {
    slug: string;
    comment: {
        body: string;
    }
}

export interface CreateCommentResponse {
    comment: Comment;
}

export interface MultipleCommentsResponse {
    comments: Comment[];
}